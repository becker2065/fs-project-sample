import React from 'react'
import { Link, } from 'react-router-dom'

import auth0Client from '../Auth'

function Navbar(props) {
  const signOut = () => {
    auth0Client.signOut()
    props.history.replace('/')
  }

  return (
    <nav className="navbar navbar-dark bg-dark fixed-top">
      <Link className="navbar-brand" to="/users">
        Users
      </Link>
      {!auth0Client.isAuthenticated() && (
        <button className="btn btn-dark" onClick={auth0Client.signIn}>
          Sign In
        </button>
      )}
      {auth0Client.isAuthenticated() && (
        <div>
          <label htmlFor="mr-2 text-white">{auth0Client.getProfile.name}</label>
          <button
            className="btn btn-dark"
            onClick={() => {
              signOut()
            }}
          >
            Sign Out
          </button>
        </div>
      )}
    </nav>
  )
}

export default (Navbar)
