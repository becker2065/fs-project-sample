import React, { useState, useEffect } from 'react'

import NewUserForm from './NewUserForm'
import UsersList from './UsersList'

const Users = () => {
    const [data, setData] = useState({ users: [] })
    const [selectedUser, setSelectedUser] = useState(null)

    const fetchData = async () => {
        const response = await fetch('http://localhost:8280/users')
        const users = await response.json()
        setData({ users })
        setSelectedUser(null)
    }

    const onUserSelected = (user) => {
        setSelectedUser(user)
    }

    const onUserSaved = () => {
        fetchData()
    }

    const deleteUser = async (userId) => {
        const response = await fetch(`http://localhost:8280/users/${userId}`, {
            method: 'DELETE', headers: {
                Accept: 'application/json'
            }
        })
        const status = response.status

        if (status !== 200) {
            console.error('Delete user failed')
            return
        }

        fetchData()
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="UserManagement">
            <UsersList users={data.users} onDeleteUser={deleteUser} onUserSelected={onUserSelected} />
            <NewUserForm onUserSaved={onUserSaved} user={selectedUser} />
        </div>
    )
}

export default Users
