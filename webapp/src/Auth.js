import auth0 from 'auth0-js'

class Auth {
    constructor() {
        this.auth0 = new auth0.WebAuth({
            clientID: 'DZ0654P1tZFb7hVleJ59TJ4BtWTjbi9K',
            domain: 'dev-1eynrbdf.auth0.com',
            responseType: 'token id_token',
            audience: 'https://fs-project-sample-api',
            redirectUri: 'http://localhost:3030/callback',
            scope: 'openid profile email'
        })
    }

    getProfile = () => {
        return this.profile
    }

    getIdToken = () => {
        return this.idToken
    }

    isAuthenticated = () => {
        return new Date().getTime() < this.expiresAt
    }

    signIn = () => {
        this.auth0.authorize()
    }

    handleAuthentication = () => {
        return new Promise((resolve, reject) => {
            this.auth0.parseHash((err, authResult) => {
                if (err) return reject(err)
                if (!authResult || !authResult.idToken) {
                    return reject(err)
                }
                this.idToken = authResult.idToken
                this.profile = authResult.idTokenPayload
                // set the time that the id token will expire at
                this.expiresAt = authResult.idTokenPayload.exp * 1000
                resolve()
            })
        })
    }

    signOut = () => {
        // clear id token, profile, and expiration
        this.idToken = null
        this.profile = null
        this.expiresAt = null
    }
}

const auth0Client = new Auth()

export default auth0Client
