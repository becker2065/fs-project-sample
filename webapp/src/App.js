import React from 'react'
import { Route } from 'react-router-dom'

import Callback from './Callback'
import Navbar from './Navbar'
import Users from './Users';
import SomeForm from './SomeForm';

import './App.css'

function App() {
  return (
    <div className="App">
      <Navbar />
      <Route exact path="/callback" component={Callback} />
      <Route exact path="/users" component={Users} />
      <Route path="/some-form" component={SomeForm} />
    </div>
  )
}

export default App
