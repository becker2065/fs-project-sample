const websitePath = './website/'
module.exports = [
    {
        method: 'GET',
        path: '/css/{file}',
        handler: (request, reply) => {
            reply.file(websitePath + 'css/' + request.params.file)
        }
    },
    {
        method: 'GET',
        path: '/script/{file}',
        handler: (request, reply) => {
            reply.file(websitePath + 'script/' + request.params.file)
        }
    },
    {
        method: 'GET',
        path: '/image/{file}',
        handler: (request, reply) => {
            reply.file(websitePath + 'img/' + request.params.file)
        }
    }
]