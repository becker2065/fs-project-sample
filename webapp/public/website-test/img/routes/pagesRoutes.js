"use strict"
const menuJSON = require('../data/menu.json')
const socialJSON = require('../data/social_links.json')
const postOne = require('../data/postOne.json')

module.exports = [
    {
        method: 'GET',
        path: '/',
        handler: (request, reply) => {
            return reply.view('front-page', { menu: menuJSON, social: socialJSON })
        }
    },
    {
        method: 'GET',
        path: '/about',
        handler: (request, reply) => {
            return reply.view('about', { menu: menuJSON, social: socialJSON })
        }
    },
    {
        method: 'GET',
        path: '/post/postOne',
        handler: (request, reply) => {
            return reply.view('post', { menu: menuJSON, social: socialJSON, postContent: postOne })
        }
    },
    {
        method: 'GET',
        path: '/{path*}',
        handler: (request, reply) => {
            return reply.view('404', { menu: menuJSON, social: socialJSON }).code(404)
        }
    }
]