
"use strict"
const Hapi = require('hapi')
const server = new Hapi.Server()
const Vision = require('vision')
const Handlebars = require('handlebars')
const Inert = require('inert')

const routesPath = './routes/'

// connection configurations
server.connection({
    port: 3000,
    host: 'localhost'
})

// vision registration
server.register(Vision, (err) => {
    if (err) throw err
})

// inert registration
server.register(Inert, (err) => {
    if (err) throw err
})

// routes for static contents css / scripts / images / etc
server.route(require(routesPath + 'staticRoutes'))


server.views({
    engines: {
        html: Handlebars
    },
    path: 'webapp/public/website/contents',
    layoutPath: 'webapp/publicwebsite',
    layout: 'webapp/public/index',
    partialsPath: 'webapp/publicwebsite/partials',
    helpersPath: 'webapp/public/website/helpers'
})

// routes for the website
server.route(require(routesPath + '/routes/pagesRoutes'))

// start serving
server.start((err) => {
    if (err) throw err

    console.log('server listening at: ', server.info.uri)
})