console.log(process.env)

export default {
  client: 'pg',
  //connection: process.env.DB_CONNECTION,
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_SCHEMA
  },
  migrations: {
    tableName: 'knex_migrations',
    directory: `${__dirname}/migrations`
  },
  pool: {
    min: 2,
    max: 10
  }
}
