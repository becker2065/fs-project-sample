import * as users from '../handlers/users'
import Joi from '@hapi/joi'

const userSchema = {
  email: Joi.string()
    .trim()
    .email()
    .required(),
  handle: Joi.string()
    .trim()
    .min(4)
    .max(50)
    .required()
}

const userRoutes = [
  {
    method: 'GET',
    path: '/users',
    handler: users.getAll,
    options: {
      auth: false
    }
  },
  {
    method: 'GET',
    path: '/users/{id}',
    handler: users.getSingle,
    options: {
      auth: false
    }
  },
  {
    method: 'POST',
    path: '/users',
    handler: users.add,
    options: {
      auth: false,
      validate: {
        payload: userSchema
      }
    }
  },
  {
    method: 'PUT',
    path: '/users/{id}',
    handler: users.update,
    options: {
      auth: false,
      validate: {
        payload: userSchema
      }
    }
  },
  {
    method: 'DELETE',
    path: '/users/{id}',
    handler: users.remove,
    options: {
      auth: 'jwt'
    }

  },

]

export default userRoutes
