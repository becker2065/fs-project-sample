import userRoutes from './users'

const nameParam = {
  path: '/{name}',
  method: 'GET',
  handler: (request) => {
    return { message: request.params.name }
  },
}

export default [nameParam, ...userRoutes]
